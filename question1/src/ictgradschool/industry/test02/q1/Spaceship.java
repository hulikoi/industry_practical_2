package ictgradschool.industry.test02.q1;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by Andrew Meads on 9/01/2018.
 */
public class Spaceship {

    private static final int SPRITE_WIDTH = 100, SPRITE_HEIGHT = 100;

    private int x, y, width, height, speed;

    private Direction direction = Direction.Up;

    private Image image;

    public Spaceship(int x, int y, int speed) {

        try {
            this.image = ImageIO.read(new File("spaceship.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.x = x;
        this.y = y;
        this.width = SPRITE_WIDTH;
        this.height = SPRITE_HEIGHT;
        this.speed = speed;

    }

    public void move(Direction direction, int windowWidth, int windowHeight) {

        // TODO Step 6.
        if(direction==Direction.None){

        } else if(direction==Direction.Left){
            if((x-speed)<0){x=0;}else {
            x=x-speed;
            this.direction=direction;}
        } else if (direction==Direction.Right){
            if((x+width+speed)>windowWidth){x=windowWidth-width;}else {
            x=x+speed;
                this.direction=direction;}
        } else if (direction==Direction.Up){
            if((y-speed)<0){y=0;}else {
            y=y-speed;
                this.direction=direction;}
        }else if (direction==Direction.Down){
            if((y+height+speed)>windowHeight){y=windowHeight-height;}else {
            y=y+speed;
                this.direction=direction;}
        }


    }

    public void paint(Graphics g) {

        // TODO Step 7 (modify this).
        switch (this.direction){
            case None:
                this.direction=Direction.None;
                return;
            case Up:
                g.drawImage(image, x, y, x + width, y + height, 0, 0, width, height, null);
                return;
            case Down:
                g.drawImage(image, x, y, x + width, y + height, width, 0, width*2, height, null);
                return;
            case Left:
                g.drawImage(image, x, y, x + width, y + height, width*2, 0, width*2+height, height, null);
                return;
            case Right:
                g.drawImage(image, x, y, x + width, y + height, width*2+height, 0, width*2+height*2, height, null);
                return;
        }

        g.drawImage(image, x, y, x + width, y + height, 0, 0, width, height, null);

    }

}

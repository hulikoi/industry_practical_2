package ictgradschool.industry.test02.q1;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew Meads on 9/01/2018.
 */
public class Starfield {

    private List<Star> stars;

    public Starfield(int width, int height) {

        // TODO Step 2a.
        stars = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            int randWidth = (int)(Math.random()*width);
            int randHeight = (int)(Math.random()*height);
            int randSize = (int)(Math.random()*4)+2;
            int randSpeed = (int)(Math.random()*6)+5;
            stars.add(new Star(randWidth,randHeight,randSize,randSpeed));
        }

    }

    public void move(int windowWidth, int windowHeight) {

        // TODO Step 2b.

        for (Star star:stars
             ) {
            star.move(windowWidth,windowHeight);
        }
    }

    public void paint(Graphics g) {

        // TODO Step 2c.
        for (Star star:stars
             ) {
            star.paint(g);
        }
    }

}

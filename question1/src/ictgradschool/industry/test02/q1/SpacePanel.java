package ictgradschool.industry.test02.q1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static java.awt.event.KeyEvent.*;

/**
 * Created by Andrew Meads on 9/01/2018.
 * <p>
 * TODO Steps 4 and 5 (implement interfaces)
 */
public class SpacePanel extends JPanel implements ActionListener, KeyListener {

    public static final int PREFERRED_WIDTH = 500;
    public static final int PREFERRED_HEIGHT = 500;

    private Starfield stars;

    private Spaceship ship;

    private Direction moveDirection = Direction.None;

    private Timer timer;

    public SpacePanel() {
        setPreferredSize(new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT));

        this.stars = new Starfield(PREFERRED_WIDTH, PREFERRED_HEIGHT);

        this.ship = new Spaceship(PREFERRED_WIDTH / 2, PREFERRED_HEIGHT / 2, 10);

        // TODO Step 4b.
        timer = new Timer(20, this);
        this.addKeyListener(this);

    }

    // TODO Step 4c (modify the two methods below).

    public void start() {
        timer.start();
    }

    public void stop() {
        timer.stop();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(Color.black);
        g.fillRect(0, 0, getWidth(), getHeight());

        // TODO Step 3.
        stars.paint(g);
        ship.paint(g);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        stars.move(PREFERRED_WIDTH, PREFERRED_HEIGHT);
        repaint();
        ship.move(moveDirection,PREFERRED_WIDTH,PREFERRED_HEIGHT);
        repaint();
        requestFocusInWindow();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case VK_UP:
                moveDirection = Direction.Up;
                return;
            case VK_DOWN:
                moveDirection = Direction.Down;
                return;
            case VK_LEFT:
                moveDirection = Direction.Left;
                return;
            case VK_RIGHT:
                moveDirection = Direction.Right;
                return;
            default:
                moveDirection = Direction.None;

    }

}

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
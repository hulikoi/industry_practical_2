package ictgradschool.industry.test02.q2.model;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

public class ThumbnailTableModelAdapter extends AbstractTableModel implements ThumbnailListener {

    private static final String[] COLUMN_NAMES = {"Image", "Name"};
    private ThumbnailList thumbnailList;


    public ThumbnailTableModelAdapter(ThumbnailList thumbnails) {

        // TODO Implement this
        this.thumbnailList = thumbnails;
        thumbnailList.addListener(this);

    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        // TODO Implement this
        if (columnIndex == 0) {
            return thumbnailList.get(rowIndex).getImage();
        } else if (columnIndex == 1) {
            return thumbnailList.get(rowIndex).getName();
        }

        return null;
    }

    @Override
    public int getRowCount() {

        // TODO Implement this
        return thumbnailList.size();
    }

    @Override
    public int getColumnCount() {

        // TODO Implement this
        return 2;
    }

    /**
     * Sets the table up to display images in its first column, and Strings in its second column.
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return ImageIcon.class;
            case 1:
                return String.class;
            default:
                throw new IllegalArgumentException();
        }
    }

    /**
     * Sets the table up with the correct column names.
     */
    @Override
    public String getColumnName(int column) {
        return COLUMN_NAMES[column];
    }

    @Override
    public void thumbnailAdded(ThumbnailList list) {
        fireTableDataChanged();
    }

    @Override
    public void thumbnailListCleared(ThumbnailList list) {
        fireTableDataChanged();
    }
}

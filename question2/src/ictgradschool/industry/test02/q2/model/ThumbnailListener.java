package ictgradschool.industry.test02.q2.model;

public interface ThumbnailListener {

    void thumbnailAdded(ThumbnailList list);

    void thumbnailListCleared(ThumbnailList list);
}
